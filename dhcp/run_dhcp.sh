#!/bin/bash
docker run -d --name dnsmasq --cap-add=NET_ADMIN --net=host --restart=always \
  -v /root/hosts:/hosts \
  -v /root/dnsmasq.leases:/var/lib/misc/dnsmasq.leases \
  quay.io/coreos/dnsmasq \
  -d -q \
  --domain=conf.cenic.org \
  --server=137.164.29.72 \
  --dhcp-range=137.164.48.201,137.164.48.251,255.255.255.0 \
  --dhcp-option=option:router,137.164.48.1 \
  --dhcp-hostsfile=/hosts \
  --enable-tftp --tftp-root=/var/lib/tftpboot \
  --dhcp-match=set:bios,option:client-arch,0 \
  --dhcp-boot=tag:bios,undionly.kpxe \
  --dhcp-match=set:efi32,option:client-arch,6 \
  --dhcp-boot=tag:efi32,ipxe.efi \
  --dhcp-match=set:efibc,option:client-arch,7 \
  --dhcp-boot=tag:efibc,ipxe.efi \
  --dhcp-match=set:efi64,option:client-arch,9 \
  --dhcp-boot=tag:efi64,ipxe.efi \
  --dhcp-userclass=set:ipxe,iPXE \
  --dhcp-boot=tag:ipxe,http://matchbox.example.com:8000/install.ipxe?mac=${mac} \
  --address=/matchbox.example.com/137.164.48.196 \
  --address=/perfsonar.conf.cenic.org/137.164.48.196 \
  --address=/gridftp-ma.conf.cenic.org/137.164.48.197 \
  --log-queries \
  --log-dhcp
